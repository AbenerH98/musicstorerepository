﻿using System;
using System.Collections.Generic;

namespace MusicStore.Models
{
    public partial class Factura
    {
        public int FacturaId { get; set; }
        public int ClienteId { get; set; }
        public DateTime FechaFactura { get; set; }
        public float Total { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
